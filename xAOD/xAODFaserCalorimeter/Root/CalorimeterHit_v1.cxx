/*
  Copyright (C) 2020 CERN for the benefit of the FASER collaboration
*/

// EDM include(s):
#include "xAODCore/AuxStoreAccessorMacros.h"

// Local include(s):
#include "xAODFaserCalorimeter/versions/CalorimeterHit_v1.h"

namespace xAOD {

  CalorimeterHit_v1::CalorimeterHit_v1() : SG::AuxElement() {
  }

  AUXSTORE_PRIMITIVE_SETTER_AND_GETTER( CalorimeterHit_v1, float, localtime, set_localtime )

  AUXSTORE_PRIMITIVE_SETTER_AND_GETTER( CalorimeterHit_v1, float, bcidtime, set_bcidtime )

  AUXSTORE_PRIMITIVE_SETTER_AND_GETTER( CalorimeterHit_v1, float, raw_energy, set_raw_energy )

  // setters and getters for the Calo WaveformHit links

  AUXSTORE_OBJECT_SETTER_AND_GETTER( CalorimeterHit_v1,
				     CalorimeterHit_v1::WaveformHitLinks_t,
				     caloWaveformLinks,
				     setCaloWaveformLinks )

  static const SG::AuxElement::Accessor< CalorimeterHit_v1::WaveformHitLinks_t > caloHitAcc( "caloWaveformLinks" );

  const WaveformHit* CalorimeterHit_v1::caloHit( size_t i ) const {
    return ( *caloHitAcc( *this )[ i ] );
  }

  size_t CalorimeterHit_v1::nCaloHits() const {
    return caloHitAcc( *this ).size();
  }

  void CalorimeterHit_v1::addCaloHit( const xAOD::WaveformHitContainer* pWaveformHitContainer, 
				      const xAOD::WaveformHit* pWaveformHit) {
    ElementLink< xAOD::WaveformHitContainer > linkToWaveformHit;
    linkToWaveformHit.toContainedElement(*pWaveformHitContainer, pWaveformHit);

    caloHitAcc( *this ).push_back( linkToWaveformHit );

    return;
  }

  void CalorimeterHit_v1::clearCaloWaveformLinks() {
    caloHitAcc( *this ).clear();
    return;
  }

} // namespace xAOD

namespace xAOD {

  std::ostream& operator<<(std::ostream& s, const xAOD::CalorimeterHit_v1& hit) {
    s << "xAODCalorimeterHit:"
      << " local time=" << hit.localtime()
      << " raw_energy=" << hit.raw_energy()
      << std::endl;

    return s;
  }

} // namespace xAOD
