/*
  FASER Collaboration
*/

// EDM include(s):
#include "xAODCore/AuxStoreAccessorMacros.h"

// Local include(s):
#include "xAODFaserTracking/StripCluster.h"

namespace xAOD {

  StripCluster::StripCluster()
    : SG::AuxElement() {    
  }

  AUXSTORE_PRIMITIVE_SETTER_AND_GETTER( StripCluster, uint64_t, id, setId)
  AUXSTORE_OBJECT_SETTER_AND_GETTER( StripCluster, std::vector< uint64_t >,
                     rdoIdentifierList,
                     setRdoIdentifierList )

  AUXSTORE_PRIMITIVE_GETTER( StripCluster, float, localX )
  AUXSTORE_PRIMITIVE_GETTER( StripCluster, float, localY )
  AUXSTORE_PRIMITIVE_GETTER( StripCluster, float, localXError )
  AUXSTORE_PRIMITIVE_GETTER( StripCluster, float, localYError )
  AUXSTORE_PRIMITIVE_GETTER( StripCluster, float, localXYCorrelation )
  
  void StripCluster::setLocalPosition(float localX, float localY) {
    static const Accessor< float > acc1( "localX" );
    acc1( *this ) = localX;
    static const Accessor< float > acc2( "localY" );
    acc2( *this ) = localY;
  }

  void StripCluster::setLocalPositionError(float localXError, float localYError, float localXYCorrelation) {
    static const Accessor< float > acc1( "localXError" );
    acc1( *this ) = localXError;
    static const Accessor< float > acc2( "localYError" );
    acc2( *this ) = localYError;
    static const Accessor< float > acc3( "localXYCorrelation" );
    acc3( *this ) = localXYCorrelation;
  }

  AUXSTORE_PRIMITIVE_GETTER( StripCluster, float, globalX )
  AUXSTORE_PRIMITIVE_GETTER( StripCluster, float, globalY )
  AUXSTORE_PRIMITIVE_GETTER( StripCluster, float, globalZ )
  
  void StripCluster::setGlobalPosition(float globalX, float globalY, float globalZ) {
    static const Accessor< float > acc1( "globalX" );
    acc1( *this ) = globalX;
    static const Accessor< float > acc2( "globalY" );
    acc2( *this ) = globalY;
    static const Accessor< float > acc3( "globalZ" );
    acc3( *this ) = globalZ;
  }


} // namespace xAOD