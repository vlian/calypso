#!/usr/bin/env python

import sys
from AthenaCommon.Logging import log, logging
from AthenaCommon.Constants import DEBUG, VERBOSE, INFO
from AthenaCommon.Configurable import Configurable
from CalypsoConfiguration.AllConfigFlags import ConfigFlags
from CalypsoConfiguration.MainServicesConfig import MainServicesCfg
from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg
from AthenaPoolCnvSvc.PoolWriteConfig import PoolWriteCfg
from TrackerPrepRawDataFormation.TrackerPrepRawDataFormationConfig import FaserSCT_ClusterizationCfg
from TrackerSpacePointFormation.TrackerSpacePointFormationConfig import TrackerSpacePointFinderCfg
from TrackerSegmentFit.TrackerSegmentFitConfig import SegmentFitAlgCfg
from FaserActsKalmanFilter.SeedingConfig import SeedingCfg

log.setLevel(DEBUG)
Configurable.configurableRun3Behavior = True

ConfigFlags.Input.Files = ['my.RDO.pool.root']
ConfigFlags.Output.ESDFileName = "circleFitSeeding.ESD.pool.root"
ConfigFlags.IOVDb.GlobalTag = "OFLCOND-FASER-01"
ConfigFlags.GeoModel.FaserVersion = "FASER-01"
ConfigFlags.GeoModel.Align.Dynamic = False
ConfigFlags.Beam.NumberOfCollisions = 0.
# ConfigFlags.TrackingGeometry.MaterialSource = "Input"
ConfigFlags.lock()

acc = MainServicesCfg(ConfigFlags)
acc.merge(PoolReadCfg(ConfigFlags))
acc.merge(PoolWriteCfg(ConfigFlags))
acc.merge(FaserSCT_ClusterizationCfg(ConfigFlags))
acc.merge(TrackerSpacePointFinderCfg(ConfigFlags))
acc.merge(SegmentFitAlgCfg(ConfigFlags, SharedHitFraction=0.51, MinClustersPerFit=5, TanThetaCut=0.25))
acc.merge(SeedingCfg(ConfigFlags))
acc.getEventAlgo("SeedingAlg").OutputLevel = VERBOSE

# logging.getLogger('forcomps').setLevel(VERBOSE)
# acc.foreach_component("*").OutputLevel = VERBOSE
# acc.foreach_component("*ClassID*").OutputLevel = VERBOSE
# acc.getService("StoreGateSvc").Dump = True
# acc.getService("ConditionStore").Dump = True
# acc.printConfig(withDetails=True)
# ConfigFlags.dump()

sc = acc.run(maxEvents=10)
sys.exit(not sc.isSuccess())
