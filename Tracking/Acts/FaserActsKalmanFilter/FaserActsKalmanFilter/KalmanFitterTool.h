#ifndef FASERACTSKALMANFILTER_KALMANFITTERTOOL_H
#define FASERACTSKALMANFILTER_KALMANFITTERTOOL_H

#include "TrackerPrepRawData/FaserSCT_ClusterContainer.h"
#include "AthenaBaseComps/AthAlgTool.h"
#include "Acts/EventData/TrackParameters.hpp"
#include "Acts/TrackFitting/KalmanFitter.hpp"
#include "FaserActsGeometryInterfaces/IFaserActsTrackingGeometryTool.h"
#include "FaserActsKalmanFilter/IndexSourceLink.h"
#include "FaserActsKalmanFilter/Measurement.h"
#include "FaserActsKalmanFilter/FaserActsRecMultiTrajectory.h"
#include "MagFieldConditions/FaserFieldCacheCondObj.h"
#include "FaserActsKalmanFilter/RootTrajectoryStatesWriterTool.h"
#include "FaserActsKalmanFilter/RootTrajectorySummaryWriterTool.h"
#include "TrkTrack/Track.h"


class FaserSCT_ID;

class KalmanFitterTool : virtual public AthAlgTool {
public:
  KalmanFitterTool(const std::string &type, const std::string &name, const IInterface *parent);
  virtual ~KalmanFitterTool() = default;
  virtual StatusCode initialize() override;
  virtual StatusCode finalize() override;

  using TrackParameters = Acts::BoundTrackParameters;
  using IndexedParams = std::unordered_map<size_t, TrackParameters>;
  using TrackFitterOptions =
      Acts::KalmanFitterOptions<MeasurementCalibrator, Acts::VoidOutlierFinder, Acts::VoidReverseFilteringLogic>;
  using TrackFitterResult = Acts::Result<Acts::KalmanFitterResult<IndexSourceLink>>;
  class TrackFitterFunction {
  public:
    virtual ~TrackFitterFunction() = default;
    virtual TrackFitterResult operator()(const std::vector<IndexSourceLink>&,
                                         const TrackParameters&,
                                         const TrackFitterOptions&) const = 0;
  };
  static std::shared_ptr<TrackFitterFunction> makeTrackFitterFunction(
      std::shared_ptr<const Acts::TrackingGeometry> trackingGeometry);

  virtual Acts::MagneticFieldContext getMagneticFieldContext(const EventContext& ctx) const;
  std::unique_ptr<Trk::Track> fit(const EventContext& ctx, const Trk::Track& inputTrack,
                                  std::vector<FaserActsRecMultiTrajectory> &trajectories,
                                  const Acts::BoundVector& inputVector, bool isMC, double origin) const;

private:
  const FaserSCT_ID* m_idHelper {nullptr};
  std::tuple<std::vector<IndexSourceLink>, std::vector<Measurement>>
  getMeasurementsFromTrack(const Trk::Track &track) const;
  // Acts::BoundTrackParameters getParametersFromTrack(const Acts::BoundVector& params, const Trk::TrackParameters *inputParameters) const;
  Acts::BoundTrackParameters getParametersFromTrack(const Trk::TrackParameters *inputParameters, const Acts::BoundVector& inputVector, double origin) const;
  std::shared_ptr<TrackFitterFunction> m_fit;
  std::unique_ptr<const Acts::Logger> m_logger;
  Gaudi::Property<std::string> m_actsLogging {this, "ActsLogging", "VERBOSE"};
  Gaudi::Property<std::size_t> m_minMeasurements {this, "MinMeasurements", 12, "minimum number of measurements of the input track"};
  Gaudi::Property<double> m_seedCovarianceScale {this, "SeedCovarianceScale", 100, "scale covariance from initial track"};
  Gaudi::Property<bool> m_isMC {this, "isMC", false};
  Gaudi::Property<bool> m_summaryWriter {this, "SummaryWriter", false};
  Gaudi::Property<bool> m_statesWriter {this, "StatesWriter", false};
  Gaudi::Property<bool> m_noDiagnostics {this, "noDiagnostics", true, "Set ACTS logging level to INFO and do not run performance writer, states writer or summary writer"};

  SG::ReadCondHandleKey<FaserFieldCacheCondObj> m_fieldCondObjInputKey {this, "FaserFieldCacheCondObj", "fieldCondObj", "Name of the Magnetic Field conditions object key"};
  ToolHandle<IFaserActsTrackingGeometryTool> m_trackingGeometryTool {this, "TrackingGeometryTool", "FaserActsTrackingGeometryTool"};
  ToolHandle<RootTrajectoryStatesWriterTool> m_trajectoryStatesWriterTool {this, "RootTrajectoryStatesWriterTool", "RootTrajectoryStatesWriterTool"};
  ToolHandle<RootTrajectorySummaryWriterTool> m_trajectorySummaryWriterTool {this, "RootTrajectorySummaryWriterTool", "RootTrajectorySummaryWriterTool"};
  std::unique_ptr<Trk::Track> makeTrack(Acts::GeometryContext& tgContext, TrackFitterResult& fitResult) const;
  const Trk::TrackParameters* ConvertActsTrackParameterToATLAS(const Acts::BoundTrackParameters &actsParameter, const Acts::GeometryContext& gctx) const;
};

#endif //FASERACTSKALMANFILTER_KALMANFITTERTOOL_H

