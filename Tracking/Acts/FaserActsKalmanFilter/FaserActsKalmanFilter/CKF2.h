#ifndef FASERACTSKALMANFILTER_CKF2_H
#define FASERACTSKALMANFILTER_CKF2_H


#include "AthenaBaseComps/AthReentrantAlgorithm.h"
#include "AthenaBaseComps/AthAlgorithm.h"
#include "TrackerSpacePoint/FaserSCT_SpacePointContainer.h"
#include "TrackerPrepRawData/FaserSCT_ClusterContainer.h"
#include "FaserActsGeometryInterfaces/IFaserActsTrackingGeometryTool.h"
#include "Acts/TrackFitting/KalmanFitter.hpp"
#include "Acts/TrackFinding/CombinatorialKalmanFilter.hpp"
#include "Acts/TrackFinding/MeasurementSelector.hpp"
#include "FaserActsKalmanFilter/Measurement.h"
#include "MagFieldConditions/FaserFieldCacheCondObj.h"
#include "FaserActsKalmanFilter/TrajectoryWriterTool.h"
#include "TrkTrack/TrackCollection.h"
#include "FaserActsKalmanFilter/ITrackSeedTool.h"
#include "FaserActsKalmanFilter/RootTrajectoryStatesWriterTool.h"
#include "FaserActsKalmanFilter/RootTrajectorySummaryWriterTool.h"
#include "FaserActsKalmanFilter/PerformanceWriterTool.h"
#include "FaserActsKalmanFilter/KalmanFitterTool.h"
#include <boost/dynamic_bitset.hpp>
using ConstTrackStateProxy = Acts::detail_lt::TrackStateProxy<IndexSourceLink, 6, true>;
using ClusterSet = boost::dynamic_bitset<>;

class FaserSCT_ID;

namespace Trk {
class TrackStateOnSurface;
}

namespace TrackerDD {
class SCT_DetectorManager;
}

class CKF2 : public AthAlgorithm {
public:
  CKF2(const std::string& name, ISvcLocator* pSvcLocator);
  virtual ~CKF2() = default;

  StatusCode initialize() override;
  StatusCode execute() override;
  StatusCode finalize() override;

  using TrackFinderOptions =
  Acts::CombinatorialKalmanFilterOptions<IndexSourceLinkAccessor,
      MeasurementCalibrator,
      Acts::MeasurementSelector>;
  using CKFResult = Acts::CombinatorialKalmanFilterResult<IndexSourceLink>;
  using TrackFitterResult = Acts::Result<CKFResult>;
  using TrackFinderResult = std::vector<TrackFitterResult>;

  using KFResult =
    Acts::Result<Acts::KalmanFitterResult<IndexSourceLink>>;

  using TrackFitterOptions =
  Acts::KalmanFitterOptions<MeasurementCalibrator, Acts::VoidOutlierFinder,
      Acts::VoidReverseFilteringLogic>;

  using TrackParameters = Acts::CurvilinearTrackParameters;
  using TrackParametersContainer = std::vector<TrackParameters>;

  // Track Finding
  class TrackFinderFunction {
  public:
    virtual ~TrackFinderFunction() = default;
    virtual TrackFinderResult operator()(const IndexSourceLinkContainer&,
                                         const TrackParametersContainer&,
                                         const TrackFinderOptions&) const = 0;
  };

  static std::shared_ptr<TrackFinderFunction> makeTrackFinderFunction(
      std::shared_ptr<const Acts::TrackingGeometry> trackingGeometry,
      bool resolvePassive, bool resolveMaterial, bool resolveSensitive);

  // Track Fitting
  class TrackFitterFunction {
  public:
    virtual ~TrackFitterFunction() = default;
    virtual KFResult operator()(const std::vector<IndexSourceLink>&,
                                const Acts::BoundTrackParameters&,
                                const TrackFitterOptions&) const = 0;
  };

  struct TrajectoryInfo {
    TrajectoryInfo(const FaserActsRecMultiTrajectory &traj) :
        trajectory{traj}, clusterSet{nClusters} {
      auto state = Acts::MultiTrajectoryHelpers::trajectoryState(traj.multiTrajectory(), traj.tips().front());
      traj.multiTrajectory().visitBackwards(traj.tips().front(), [&](const ConstTrackStateProxy& state) {
        auto typeFlags = state.typeFlags();
        if (not typeFlags.test(Acts::TrackStateFlag::MeasurementFlag)) {
          return true;
        }
        clusterSet.set(state.uncalibrated().index());
        return true;
      });
      nMeasurements = state.nMeasurements;
      chi2 = state.chi2Sum;
    }

    static size_t nClusters;
    FaserActsRecMultiTrajectory trajectory;
    ClusterSet clusterSet;
    size_t nMeasurements;
    double chi2;
  };

  static std::shared_ptr<TrackFitterFunction> makeTrackFitterFunction(
      std::shared_ptr<const Acts::TrackingGeometry> trackingGeometry);

  virtual Acts::MagneticFieldContext getMagneticFieldContext(const EventContext& ctx) const;

private:
  void computeSharedHits(std::vector<IndexSourceLink>* sourceLinks, TrackFinderResult& results) const;
  std::shared_ptr<TrackFinderFunction> m_fit;
  std::shared_ptr<TrackFitterFunction> m_kf;
  std::unique_ptr<const Acts::Logger> m_logger;
  const FaserSCT_ID* m_idHelper {nullptr};
  const TrackerDD::SCT_DetectorManager* m_detManager {nullptr};

  Gaudi::Property<std::string> m_actsLogging {this, "ActsLogging", "VERBOSE"};
  Gaudi::Property<int> m_minNumberMeasurements {this, "MinNumberMeasurements", 12};
  Gaudi::Property<bool> m_backwardPropagation {this, "BackwardPropagation", false};
  Gaudi::Property<bool> m_performanceWriter {this, "PerformanceWriter", true};
  Gaudi::Property<bool> m_summaryWriter {this, "SummaryWriter", true};
  Gaudi::Property<bool> m_noDiagnostics {this, "noDiagnostics", true, "Set ACTS logging level to INFO and do not run performance writer, states writer or summary writer"};
  Gaudi::Property<bool> m_statesWriter {this, "StatesWriter", false};
  Gaudi::Property<bool> m_resolvePassive {this, "resolvePassive", false};
  Gaudi::Property<bool> m_resolveMaterial {this, "resolveMaterial", true};
  Gaudi::Property<bool> m_resolveSensitive {this, "resolveSensitive", true};
  Gaudi::Property<double> m_maxSteps {this, "maxSteps", 10000};
  Gaudi::Property<double> m_chi2Max {this, "chi2Max", 15};
  Gaudi::Property<unsigned long> m_nMax {this, "nMax", 10};
  SG::ReadCondHandleKey<FaserFieldCacheCondObj> m_fieldCondObjInputKey {this, "FaserFieldCacheCondObj", "fieldCondObj", "Name of the Magnetic Field conditions object key"};
  ToolHandle<ITrackSeedTool> m_trackSeedTool {this, "TrackSeed", "ClusterTrackSeedTool"};
  ToolHandle<IFaserActsTrackingGeometryTool> m_trackingGeometryTool {this, "TrackingGeometryTool", "FaserActsTrackingGeometryTool"};
  ToolHandle<PerformanceWriterTool> m_performanceWriterTool {this, "PerformanceWriterTool", "PerformanceWriterTool"};
  ToolHandle<RootTrajectoryStatesWriterTool> m_trajectoryStatesWriterTool {this, "RootTrajectoryStatesWriterTool", "RootTrajectoryStatesWriterTool"};
  ToolHandle<RootTrajectorySummaryWriterTool> m_trajectorySummaryWriterTool {this, "RootTrajectorySummaryWriterTool", "RootTrajectorySummaryWriterTool"};
  ToolHandle<KalmanFitterTool> m_kalmanFitterTool1 {this, "KalmanFitterTool1", "KalmanFitterTool"};
  ToolHandle<KalmanFitterTool> m_kalmanFitterTool2 {this, "KalmanFitterTool2", "KalmanFitterTool"};
  Gaudi::Property<bool> m_isMC {this, "isMC", false};

  std::unique_ptr<Trk::Track> makeTrack(Acts::GeometryContext& tgContext, TrackFitterResult& fitResult) const;
  std::unique_ptr<Trk::Track> makeTrack(const Acts::GeometryContext &geoCtx, const FaserActsRecMultiTrajectory &traj) const;
  const Trk::TrackParameters* ConvertActsTrackParameterToATLAS(const Acts::BoundTrackParameters &actsParameter, const Acts::GeometryContext& gctx) const;
  SG::WriteHandleKey<TrackCollection> m_trackCollection { this, "CKFTrackCollection", "CKFTrackCollection" };
};

#endif // FASERACTSKALMANFILTER_CKF2_H

