#!/bin/bash
# Used with a condor file to submit to vanilla universe
#
# Usage:
# submit_faserMDC_digi.sh filepath [release_directory] [working_directory] 
# 
# filepath - full file name (with path)
# release_directory - optional path to release install directory (default pwd)
# working_directory - optional path to output directory location (default pwd)
#
# The release directory must already be set up 
# (so an unqualified asetup can set up the release properly)
#
# Script will use git describe to find the release tag.  
# If this matches gen/g???? it will be passed to the job
#
#----------------------------------------
# Keep track of time
SECONDS=0
#
# Parse command-line options
file_path=${1}
release_directory=${2}
working_directory=${3}
#
# Set defaults if arguments aren't provided
if [ -z "$file_path" ]
then
  echo "No file specified!"
  echo "Usage: submit_faserMDC_digi.sh file [release dir] [output dir]"
  exit 1
fi
#
if [ -z "$release_directory" ]
then
  release_directory=`pwd`
fi
#
if [ -z "$working_directory" ]
then
  working_directory=`pwd`
fi
#
starting_directory=`pwd`
#
# Now extract the run number and file stem
#
# First, get the filename
file_name=$(basename "$file_path")
# 
# Now split based on '.' to get stem
defaultIFS=$IFS
IFS='.'
read file_stem ext <<< "$file_name"
#
# Finally extract the run number
IFS='-'
# Read the split words into an array based on delimiter
read faser short run_number segment <<< "$file_stem"
#
# Set the IFS delimeter back or else echo doesn't work...
IFS=$defaultIFS
#
# Make output directory if needed
output_directory="$working_directory/$run_number"
mkdir -p "$output_directory"
#
# This magic redirects everything in this script to our log file
exec >& "$output_directory/$file_stem.log"
echo `date` - $HOSTNAME
echo "File: $file_name"
echo "Release: $release_directory"
echo "Output: $output_directory"
echo "Starting: $starting_directory"
#
# Set up the release (do this automatically)?
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh 
#
# Try automatic
# Always go back to the starting directory in case paths are relative
cd "$starting_directory"
cd "$release_directory"
# asetup
# source build/x8*/setup.sh
#
# Do this by hand
asetup --input=calypso/asetup.faser Athena,22.0.49
source build/x86*/setup.sh
#
#
# Try to find a release tag
cd calypso
recotag=`git describe`
if [[ "$recotag" == "reco/r"???? ]]; then
  tag=`echo "$recotag" | cut -c 6-11`
  echo "Found reco tag: $tag"
fi
if [[ "$recotag" == "digi/d"???? ]]; then
  tag=`echo "$recotag" | cut -c 6-11`
  echo "Found digi tag: $tag"
fi
if [[ "$recotag" == "sim/s"???? ]]; then
  tag=`echo "$recotag" | cut -c 5-10`
  echo "Found sim tag: $tag"
fi
#
# Move to the run directory
cd "$starting_directory"
cd "$output_directory"
#
# Remove any previous directory if it exists
#if [[ -e "$file_stem" ]]; then
#    echo "Remove previous directory $file_stem"
#    rm -rf "$file_stem"
#fi
#
# Make run directory
if [[ -e "$file_stem" ]]; then
    echo "Directory $file_stem already exists"
else
    mkdir "$file_stem"
fi
cd "$file_stem"
#
# Run job
if [[ -z "$tag" ]]; then
    faserMDC_digi.py "$file_path"
else
    faserMDC_digi.py "--tag=$tag" "$file_path"
fi
#
# Print out ending time
date
echo "Job finished after $SECONDS seconds"
