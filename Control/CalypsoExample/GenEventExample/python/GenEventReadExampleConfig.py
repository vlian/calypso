# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration

#!/usr/bin/env python
import sys
from AthenaConfiguration.ComponentFactory import CompFactory
from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator

def GenEventReadExampleCfg(flags, name="GenEventReadExampleAlg", **kwargs):

    a = ComponentAccumulator()

    GenEventReadAlg = CompFactory.GenEventReadAlg
    a.addEventAlgo(GenEventReadAlg(name, **kwargs))

    thistSvc = CompFactory.THistSvc()
    thistSvc.Output += ["HIST DATAFILE='genEventHist.root' OPT='RECREATE'"]
    a.addService(thistSvc)
    return a


if __name__ == "__main__":
    from AthenaCommon.Configurable import Configurable
    from CalypsoConfiguration.AllConfigFlags import ConfigFlags

    Configurable.configurableRun3Behavior = True
    
# Flags for this job
    ConfigFlags.Input.isMC = True                                # Needed to bypass autoconfig
    ConfigFlags.IOVDb.GlobalTag = "OFLCOND-FASER-01"             # Always needed; must match FaserVersion
    ConfigFlags.GeoModel.FaserVersion     = "FASER-01"           # Default FASER geometry
    ConfigFlags.Detector.EnableFaserSCT = True
    ConfigFlags.Input.Files = ["my.cosmics.pool.root"]
    ConfigFlags.lock()

# Configure components
    from CalypsoConfiguration.MainServicesConfig import MainServicesCfg
    from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg
    acc = MainServicesCfg(ConfigFlags)
    acc.merge(PoolReadCfg(ConfigFlags))

# Set things up to create a conditions DB with neutral Tracker alignment transforms
    acc.merge(GenEventReadExampleCfg(ConfigFlags))

# Configure verbosity    
    # ConfigFlags.dump()
    # logging.getLogger('forcomps').setLevel(VERBOSE)
    # acc.foreach_component("*").OutputLevel = VERBOSE
    # acc.foreach_component("*ClassID*").OutputLevel = INFO
    # log.setLevel(VERBOSE)
    
# Execute and finish
    sys.exit(int(acc.run(maxEvents=-1).isFailure()))
