#include "MyExtrapolationExample.h"
#include "FaserActsGeometryInterfaces/IFaserActsTrackingGeometryTool.h"
#include "GeoPrimitives/CLHEPtoEigenConverter.h"
#include "Acts/Surfaces/PerigeeSurface.hpp"
#include "TrackerIdentifier/FaserSCT_ID.h"
#include "TrackerReadoutGeometry/SCT_DetectorManager.h"
#include "TrackerReadoutGeometry/SiDetectorElement.h"
#include <cmath>


namespace Tracker {

MyExtrapolationExample::MyExtrapolationExample(const std::string &name, ISvcLocator *pSvcLocator)
    : AthReentrantAlgorithm(name, pSvcLocator) {}

StatusCode MyExtrapolationExample::initialize() {
  ATH_CHECK(m_mcEventCollectionKey.initialize());
  ATH_CHECK(m_faserSiHitKey.initialize());
  ATH_CHECK(m_extrapolationTool.retrieve());
  ATH_CHECK(detStore()->retrieve(m_idHelper, "FaserSCT_ID"));
  ATH_CHECK(detStore()->retrieve(m_detMgr, "SCT"));
  return StatusCode::SUCCESS;
}

StatusCode MyExtrapolationExample::execute(const EventContext &ctx) const {
  const Acts::GeometryContext gctx =
      m_extrapolationTool->trackingGeometryTool()->getNominalGeometryContext().context();

  std::vector<double> z_positions {};
  SG::ReadHandle<FaserSiHitCollection> siHitCollection {m_faserSiHitKey, ctx};
      ATH_CHECK(siHitCollection.isValid());
  for (const FaserSiHit& hit : *siHitCollection) {
    if ((hit.getStation() == 1) && (hit.getPlane() == 0) && (hit.getSensor() == 0)) {
      if (hit.particleLink()) {
        if (std::abs(hit.particleLink()->pdg_id()) != 13)
          continue;
        Identifier id = m_idHelper->wafer_id(hit.getStation(), hit.getPlane(), hit.getRow(), hit.getModule(), hit.getSensor());
        const TrackerDD::SiDetectorElement* element = m_detMgr->getDetectorElement(id);
        const HepGeom::Point3D<double> globalStartPosition =
            Amg::EigenTransformToCLHEP(element->transformHit()) * hit.localStartPosition();
        z_positions.push_back(globalStartPosition.z());
        ATH_MSG_DEBUG("SiHit: " << globalStartPosition.x() << ", " << globalStartPosition.y() << ", " << globalStartPosition.z());
      }
    }
  }

  double z_mean = 0;
  for (double z : z_positions) {
    z_mean += z;
  }
  z_mean /= z_positions.size();

  SG::ReadHandle<McEventCollection> mcEvents {m_mcEventCollectionKey, ctx};
      ATH_CHECK(mcEvents.isValid());
  if (mcEvents->size() != 1) {
    ATH_MSG_ERROR("There should be exactly one event in the McEventCollection.");
    return StatusCode::FAILURE;
  }


  for (const HepMC::GenParticle* particle : mcEvents->front()->particle_range()) {
    if ((std::abs(particle->pdg_id()) != 13)) {
      continue;
    }
    const HepMC::FourVector& vertex = particle->production_vertex()->position();
    if (vertex.z() > 0) {
      continue;
    }
    const HepMC::FourVector& momentum = particle->momentum();
    double phi = momentum.phi();
    double theta = momentum.theta();
    double charge = particle->pdg_id() > 0 ? -1 : 1;
    double abs_momentum = momentum.rho() * m_MeV2GeV;
    double qop = charge / abs_momentum;
    // The coordinate system of the Acts::PlaneSurface is defined as
    // T = Z = normal, U = X x T = -Y, V = T x U = x
//    Acts::BoundVector pars;
//    pars << -vertex.y(), vertex.x(), phi, theta, qop, vertex.t();
    Acts::BoundVector pars = Acts::BoundVector::Zero();
    pars[Acts::eBoundLoc0] = -vertex.y();
    pars[Acts::eBoundLoc1] = vertex.x();
    pars[Acts::eBoundPhi] = phi;
    pars[Acts::eBoundTheta] = theta;
    pars[Acts::eBoundQOverP] = qop;
    pars[Acts::eBoundTime] = vertex.t();

    auto startSurface = Acts::Surface::makeShared<Acts::PlaneSurface>(
        Acts::Vector3(0, 0, vertex.z()), Acts::Vector3(0, 0, 1));
    auto targetSurface = Acts::Surface::makeShared<Acts::PlaneSurface>(
        Acts::Vector3(0, 0, z_mean), Acts::Vector3(0, 0, 1));
    Acts::BoundTrackParameters startParameters(
        std::move(startSurface), pars, charge);
    ATH_MSG_DEBUG("vertex: " << vertex.x() << ", " << vertex.y() << ", " << vertex.z());
    ATH_MSG_DEBUG("vertex momentum: " << momentum.x() * m_MeV2GeV << ", " << momentum.y() * m_MeV2GeV << ", " << momentum.z() * m_MeV2GeV);
    std::unique_ptr<const Acts::BoundTrackParameters> targetParameters =
        m_extrapolationTool->propagate(ctx, startParameters, *targetSurface);
    if (targetParameters) {
      Acts::Vector3 targetPosition = targetParameters->position(gctx);
      Acts::Vector3 targetMomentum = targetParameters->momentum();
      ATH_MSG_DEBUG("vertex: " << vertex.x() << ", " << vertex.y() << ", " << vertex.z());
      ATH_MSG_DEBUG("origin: " << targetPosition.x() << ", " << targetPosition.y() << ", " << targetPosition.z());
      ATH_MSG_DEBUG("vertex momentum: " << momentum.x() * m_MeV2GeV << ", " << momentum.y() * m_MeV2GeV << ", " << momentum.z() * m_MeV2GeV);
      ATH_MSG_DEBUG("origin momentum: " << targetMomentum.x() << ", " << targetMomentum.y() << ", " << targetMomentum.z());
    }
  }

  return StatusCode::SUCCESS;
}

StatusCode MyExtrapolationExample::finalize() {
  return StatusCode::SUCCESS;
}

} // Tracker
