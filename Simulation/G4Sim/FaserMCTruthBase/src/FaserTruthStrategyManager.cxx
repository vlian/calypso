/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

// class header
#include "FaserMCTruthBase/FaserTruthStrategyManager.h"

// Framework includes
#include "AthenaBaseComps/AthMsgStreamMacros.h"

// Geant4 Includes
#include "G4Event.hh"
#include "G4EventManager.hh"
#include "G4PhysicalVolumeStore.hh"
#include "G4Step.hh"
#include "G4TransportationManager.hh"
#include "G4VPhysicalVolume.hh"
#include "G4VSolid.hh"

// Truth-related includes
#include "FaserMCTruth/FaserEventInformation.h"
// #include "FaserMCTruth/FaserTrackHelper.h"

// ISF includes
#include "FaserISF_Interfaces/IFaserTruthSvc.h"
#include "FaserISF_Event/FaserISFParticle.h"

// DetectorDescription
#include "FaserDetDescr/FaserRegionHelper.h"
#include "FaserISF_Geant4Event/FaserGeant4TruthIncident.h"
#include "FaserISF_Geant4Event/FaserISFG4GeoHelper.h"

FaserTruthStrategyManager::FaserTruthStrategyManager()
  : m_truthSvc(nullptr)
{
}

FaserTruthStrategyManager* FaserTruthStrategyManager::GetStrategyManager()
{
  static FaserTruthStrategyManager theMgr;
  return &theMgr;
}

void FaserTruthStrategyManager::SetISFTruthSvc(ISF::IFaserTruthSvc *truthSvc)
{
  m_truthSvc = truthSvc;
}

bool FaserTruthStrategyManager::CreateTruthIncident(const G4Step* aStep, int subDetVolLevel) const
{
  FaserDetDescr::FaserRegion geoID = iGeant4::FaserISFG4GeoHelper::nextGeoId(aStep, subDetVolLevel);

  auto* eventInfo = static_cast<FaserEventInformation*> (G4EventManager::GetEventManager()->GetConstCurrentEvent()->GetUserInformation());

  iGeant4::FaserGeant4TruthIncident truth(aStep, geoID, eventInfo);

  m_truthSvc->registerTruthIncident(truth);
  return false;
}

