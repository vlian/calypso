from AthenaPython.PyAthena import StatusCode, McEventCollection, HepMC, CLHEP
from GeneratorModules.EvgenAnalysisAlg import EvgenAnalysisAlg

import ROOT as R
import numpy as np
import os

def fix():
    "Python Fixes for HepMC"
    def add(self, other):
        self.set(self.x() + other.x(), self.y() + other.y(),
                 self.z() + other.z(), self.t() + other.t())
        return self

    HepMC.FourVector.__iadd__ = add
    del add

    return

class HistSvc(object):
    "Class to deal with histograms"

    def __init__(self):
        self.hists = {}

    def add(self, name, nbinsX = None, loX = None, hiX = None, nbinsY = None, loY = None, hiY = None, title = None, arrayX = None, arrayY = None):
        hname = os.path.basename(name)

        if title is None:  title = hname

        if nbinsY is not None:
            self.hists[name] = R.TH2F(hname, title, nbinsX, loX, hiX, nbinsY, loY, hiY)
        elif arrayX is not None and arrayY is not None:
            self.hists[name] = R.TH2F(hname, title, len(arrayX) - 1, arrayX, len(arrayY) - 1, arrayY)
        elif arrayX is not None and arrayY is None and nbinsY is not None:
            self.hists[name] = R.TH2F(hname, title, len(arrayX) - 1, arrayX, nbinsY, loY, hiY)
        elif arrayX is None and arrayY is not None:
            self.hists[name] = R.TH2F(hname, title, nbinsX, loX, hiX, len(arrayY) - 1, arrayY)            
        elif arrayX is not None:
            self.hists[name] = R.TH1F(hname, title, len(arrayX) - 1, arrayX)
        else:
            self.hists[name] = R.TH1F(hname, title, nbinsX, loX, hiX)                

    def __getitem__(self, name):
        return self.hists[name]

    def write(self, name):

        f = R.TFile.Open(name, "RECREATE")
    
        for n, h in self.hists.items():
            path = os.path.dirname(n)
            if path and not f.GetDirectory(path):
                f.mkdir(path)
            
            f.cd(path)
            h.Write()

        f.Close()

        return

class EvgenValidation(EvgenAnalysisAlg):
    "Gen-level validation"

    def __init__(self, name = "EvgenValidation", ndaughters = 2, outname = "validation.root"):
        super(EvgenValidation, self).__init__(name=name)
        self.hists = HistSvc()
        self.ndaughters = ndaughters
        self.outname = outname

    def binning(self):
        "binning for theta vs phi plot"
        tmin, tmax, tnum = [-6, 0, 24]
        pmin, pmax, pnum = [ 0, 5, 10]
        t_edges = np.logspace(tmin, tmax, num=tnum+1)
        p_edges = np.logspace(pmin, pmax, num=pnum+1)
        return t_edges, p_edges

    def initialize(self):

        # All daughters
        self.hists.add("PIDs", 60, -30, 30)

        # Daughter i
        tbins, pbins = self.binning()
        for i in range(self.ndaughters):
            self.hists.add(f"E_d{i}", 100, 0, 10000)                        
            self.hists.add(f"P_d{i}", 100, 0, 10000)
            self.hists.add(f"Pz_d{i}", 100, 0, 10000)
            self.hists.add(f"Pt_d{i}", 100, 0, 1)
            self.hists.add(f"Theta_d{i}", 20, 0, 0.001)
            self.hists.add(f"Phi_d{i}", 16, -3.2, 3.2)
            self.hists.add(f"ThetaVsP_d{i}", arrayX = tbins, arrayY = pbins)
            self.hists.add(f"Mass_d{i}", 200, 0, 0.01)            

        # Mother
        self.hists.add("E_M", 100, 0, 10000)
        self.hists.add("P_M", 100, 0, 10000)
        self.hists.add("Pz_M", 100, 0, 10000)         
        self.hists.add("Pt_M", 100, 0, 1)       
        self.hists.add("Theta_M", 20, 0, 0.001)
        self.hists.add("Phi_M", 16, -3.2, 3.2)
        self.hists.add("Mass_M", 200, 0, 1)
        self.hists.add("ThetaVsP_M", arrayX = tbins, arrayY = pbins)

        # Vertex
        self.hists.add("Vtx_X", 50, -100, 100)
        self.hists.add("Vtx_Y", 50, -100, 100)
        # For fluka
        #self.hists.add("Vtx_X", 100, -3000, 3000)
        #self.hists.add("Vtx_Y", 100, -3000, 3000)        
        self.hists.add("Vtx_Z", 50, -1500, 0)
        self.hists.add("Vtx_XY", 50, -100, 100, 50, -100, 100)        
        
        return StatusCode.Success


    def fillKin(self, label, p, mass = True, twoD = True):

        self.hists[f"E_{label}"].Fill(p.t()/1000, self.weight)        
        self.hists[f"P_{label}"].Fill(p.rho()/1000, self.weight)
        self.hists[f"Pz_{label}"].Fill(p.pz()/1000, self.weight)                
        self.hists[f"Pt_{label}"].Fill(p.perp()/1000, self.weight)
        self.hists[f"Theta_{label}"].Fill(p.theta(), self.weight)
        self.hists[f"Phi_{label}"].Fill(p.phi(), self.weight)

        if mass:
            self.hists[f"Mass_{label}"].Fill(p.m()/1000, self.weight)

        if twoD:
            self.hists[f"ThetaVsP_{label}"].Fill(p.theta(), p.rho()/1000, self.weight)

        return

    def fillDaughter(self, p):
        self.hists["PIDs"].Fill(p.pdg_id(), self.weight)
        return

    def fillVertex(self, v):
        self.hists["Vtx_X"].Fill(v.x(), self.weight)
        self.hists["Vtx_Y"].Fill(v.y(), self.weight)
        self.hists["Vtx_Z"].Fill(v.z(), self.weight)
        self.hists["Vtx_XY"].Fill(v.x(), v.y(), self.weight)        
        return
    

    def execute(self):
        evt = self.events()[0]
        self.weight = evt.weights()[0]

        # Loop over all particles in events (assuming mother not stored)
        momenta = []
        mother = HepMC.FourVector(0,0,0,0)
        llp_vtx = None
        for i, p in enumerate(evt.particles):
            #p.print()
            self.fillDaughter(p)
            momenta.append(p.momentum())    
            mother += p.momentum()
            if i == 0:
                #p.production_vertex().print()
                llp_vtx = p.production_vertex().point3d()

        # Fill daughter plots
        for i in range(self.ndaughters):
            self.fillKin(f"d{i}", momenta[i])

        # Fill mother plots
        self.fillKin("M", mother, mass = True)

        # Fill vertex plots
        self.fillVertex(llp_vtx)
            
        return StatusCode.Success

    def finalize(self):
        self.hists.write(self.outname)
        return StatusCode.Success

        
if __name__ == "__main__":

    import argparse, sys
    parser = argparse.ArgumentParser(description="Run gen-level validation")
    parser.add_argument("file", nargs="+", help = "full path to imput file")
    parser.add_argument("--ndaugthers", "-d", default = 2, type = int, help = "Number of daugthers to plot")
    parser.add_argument("--output", "-o",  default = "validation.root", help = "Name of output file")
    parser.add_argument("--mcEventKey", "-k",  default = "BeamTruthEvent", help = "Name of MC collection")
    parser.add_argument("--nevents", "-n", default = -1, type = int, help = "Number of events to process")    
    args = parser.parse_args()    

    from AthenaCommon.Logging import log
    from AthenaCommon.Constants import DEBUG
    log.setLevel(DEBUG)
    
    from AthenaCommon.Configurable import Configurable
    Configurable.configurableRun3Behavior = 1

    from CalypsoConfiguration.AllConfigFlags import ConfigFlags
    ConfigFlags.Input.isMC = True
    ConfigFlags.IOVDb.GlobalTag = "OFLCOND-FASER-01"             # Always needed; must match FaserVersion
    ConfigFlags.GeoModel.FaserVersion     = "FASER-01"           # Default FASER geometry
    ConfigFlags.Detector.EnableFaserSCT = True
    ConfigFlags.Input.Files = args.file
    ConfigFlags.lock()

    from CalypsoConfiguration.MainServicesConfig import MainServicesCfg
    cfg = MainServicesCfg(ConfigFlags)
    
    from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg
    cfg.merge(PoolReadCfg(ConfigFlags))

    from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
    from AthenaConfiguration.ComponentFactory import CompFactory

    import McParticleEvent.Pythonizations
    fix()
    
    acc = ComponentAccumulator()
    valid = EvgenValidation("EvgenValidation", ndaughters =  args.ndaugthers, outname = args.output)
    valid.McEventKey = args.mcEventKey
    acc.addEventAlgo(valid)    
    cfg.merge(acc)

    sc = cfg.run(maxEvents = args.nevents)
    sys.exit(not sc.isSuccess())
