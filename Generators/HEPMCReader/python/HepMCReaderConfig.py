#!/usr/bin/env python

# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration

# import sys
from AthenaConfiguration.MainServicesConfig import AthSequencer
from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.ComponentFactory import CompFactory

from TruthIO.TruthIOConf import HepMCReadFromFile


def HepMCReaderCfg(ConfigFlags, **kwargs) :
    cfg = ComponentAccumulator(AthSequencer("AthBeginSeq", Sequential = True))

    
    from TruthIO.TruthIOConf import HepMCReadFromFile
    hepmc = CompFactory.HepMCReadFromFile(name = kwargs.setdefault("name", "FASERHepMCReader"))
    hepmc.InputFile = ConfigFlags.Input.Files[0] 
    hepmc.McEventKey = kwargs.setdefault("McEventKey", "BeamTruthEvent")
    
    cfg.addEventAlgo(hepmc, sequenceName = "AthBeginSeq", primary = True) # to run *before* G4

    return cfg
